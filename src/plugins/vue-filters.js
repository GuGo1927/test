import Vue from 'vue';

Vue.filter('minText66', function (value) {
    if (value.length > 66) {
        return value.substr(0, 66) + "..."
    }else {
        return value
    }
});
Vue.filter('minText16', function (value) {
    if (value.length > 16) {
        return value.substr(0, 16) + "..."
    }else {
        return value
    }
});

