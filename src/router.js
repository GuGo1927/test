import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router);

let router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'chat',
      component: () => import('./views/ChatList.vue'),
      children: [
        {
          path: '/:id',
          name: 'chat',
          component: () => import('./views/Messages.vue'),
        }
      ]
    }
  ]
});


export default router;
