import * as mutation_types from "./mutation_types";

export default {
    // [mutation_types.M_SET_LANG](state, payload) {
    //     if (state.locales[payload]) {
    //         state.locale = payload
    //     }
    // },
    [mutation_types.M_GET_ALERT](state, payload) {
        state.alert_title = payload.title,
        state.alert_status = payload.status,
        state.alert_show = true
        setTimeout(function () {
            state.alert_show = false
        },2000)
    },
};
