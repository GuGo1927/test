import * as types from "./types";
import * as mutation_types from "./mutation_types";

export default {
    [types.A_ADD_MESSAGE](context, payload) {
        return new Promise((resolve, reject) => {
            context.commit(mutation_types.M_ADD_MESSAGE, payload);
        })
    },
}
