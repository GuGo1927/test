export default {
    chats: (state) => {
        return state.chats;
    },
    messagesById: (state) => (id) => {
        let messages = state.chats.filter(val => {
            if(val.id == id) {
                return val.parts;
            }
        });
        return messages
    },
    getAllMessagesCount: (state) => {
        let chats = state.chats;
        let messagesCount = 0;
        chats.filter( val => {
            messagesCount += val.parts.length
        });
        return messagesCount
    }
}
