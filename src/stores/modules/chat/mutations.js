import * as mutation_types from "./mutation_types";

export default {
    [mutation_types.M_ADD_MESSAGE](state, payload) {
        state.chats.filter( (val, key) => {
            if (val.id == payload.id){
                state.chats[key].parts.push(payload.data);
            }
        })
    },
};
